<?php

namespace App\Http\Controllers;

use App\{Game, Player};
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function start(Request $request, $group)
    {
        $players = Player::where('group', $group);

        $game = new Game();
        $game->num_players = $players->count();
        $game->setStatus(Game::STATUS_RUNNING);
        $game->save();
        return [];
    }
}
