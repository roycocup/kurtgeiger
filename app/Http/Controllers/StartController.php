<?php

namespace App\Http\Controllers;

use \DB;
use App\{Player, Game};
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class StartController extends Controller
{
    public function setup(Request $request)
    {
        if ($request->isMethod('post'))
        {
            DB::beginTransaction();
            
            $players = $request->all()['player'];

            $group = Uuid::generate()->string;
            foreach($players as $player)
            {
                if ($player['name'] == null || $player['email'] == null) continue;
                $dbPlayer = new Player();
                $dbPlayer->name = $player['name'];
                $dbPlayer->email = $player['email'];
                $dbPlayer->group = $group;
                $dbPlayer->save();
            }
            $game = new Game();
            $game->num_players = $request->get('num_players');
            $game->setStatus(Game::STATUS_SETUP);
            $game->save();

            DB::commit();
            return redirect()->action('GameController@start', compact('group'));
        }

        return view('setup');
    }
}
