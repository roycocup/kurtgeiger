<?php

namespace App; 

interface IRollable 
{
    public function roll();
}