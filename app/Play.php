<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Play extends Model implements IPlayable
{ 

    public static function createPlay(int $gameId, int $playerId)
    {
        $play = new Play();
        $play->game_id = $gameId;
        $play->player_id = $playerId;
        $play->save();

        return $play;
        
    }

    //TODO - Needs unit testing
    public function getSumOfEachDie()
    {
        $result = Play::where('game_id', $this->game_id)
            ->where('player_id', $this->player_id)
            ->get();

        // if there isnt a result, just send 0
        if ($result->count() < 1)
            return ['sumD1' => 0, 'sumD2' => 0];

        // get the sum of all die1 scores
        $sumD1 = 0;
        $d1 = $result->map(function($item, $key) use (&$sumD1){
            $sumD1 += $item->die1;
        });

        // get the sum of all die2 scores
        $sumD2 = 0;
        $d2 = $result->map(function($item, $key) use (&$sumD2){
            $sumD2 += $item->die2;
        });

        return ['sumD1' => $sumD1, 'sumD2' => $sumD2];
    }

    //TODO - Needs unit testing
    public function getTotalScoreForPlayerAndGame()
    {
        $result = $this->getSumOfEachDie();

        // return the total score
        return $result['sumD1'] + $result['sumD2'];
        
    }
    
    public function checkForSnakeEyes(&$result)
    {
        if ($result['die1'] == 1 && $result['die2'] == 1)
        {
            // get all the previous scores on each die and turn into negative
            $dieSums = $this->getSumOfEachDie();
            $result['die1'] = -1*$dieSums['sumD1'];
            $result['die2'] = -1*$dieSums['sumD2'];
        }
    }

    public function roll(IRollable $dice)
    {
        $result = $dice->roll();

        $this->checkForSnakeEyes($result);

        $this->die1 = $result['die1'];
        $this->die2 = $result['die2'];

        $this->save();

        return $this;
    }

    public function getPlayScore()
    {
        $d1 = $this->die1; 
        $d2 = $this->die2;

        if ($d1 == 1 || $d2 == 1)
        {
            return 0;
        } else {
            return $d1 + $d2;
        }
    }
}
