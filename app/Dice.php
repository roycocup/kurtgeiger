<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dice extends Model implements IRollable
{
    protected $table = 'dice';

    public function roll()
    {
        $dice1 = rand(1,6);
        $dice2 = rand(1,6);

        return ['die1' => $dice1, 'die2' => $dice2];
    }
}
