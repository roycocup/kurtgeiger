<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Player extends Model
{
    public function rollDice(Game $game, IRollable $dice)
    {
        $play = Play::createPlay($game->id, $this->id);
        $result = $play->roll($dice);
        return $result;
    }

    public function play()
    {
        return $this->hasOne(Play::class);
    }
}
