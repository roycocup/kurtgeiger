<?php

namespace App;

interface IPlayable
{
    public static function createPlay(int $gameId, int $playerId);
}