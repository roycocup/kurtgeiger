<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    const STATUS_SETUP      = 'setup'; // players are going through game setup phase
    const STATUS_RUNNING    = 'running'; // game is under way
    const STATUS_OVER       = 'over'; // game is over

    public function players()
    {
        return $this->belongsToMany(Player::class);
    }

    public function isGameOver()
    {
        return true;
    }

    public function hasMinNumPlayers()
    {
        if ($this->num_players >= 2) 
            return true;
        else
            return false;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setGroup(string $group)
    {
        $this->group = $group;
        $this->save();
    }

    public function setPlayers()
    {
        $players = Player::where('group', $this->group)->get();
        $this->players()->sync($players);
    }

    public function start(string $group = null)
    {
        if ($group){
            $this->setGroup($group);
            $this->setPlayers();
        }
        
        $this->cur_player = $this->players->first();
        $this->setStatus(Game::STATUS_RUNNING);
    }

    public function score(Player $player)
    {
        $play = Play::createPlay($this->id, $player->id);
        return $play->getTotalScoreForPlayerAndGame();
    }

    public function save(array $options = [])
    {
        if (!$this->getStatus())
        {
            $this->setStatus(Game::STATUS_SETUP);
        }
        parent::save($options);
    }
}
