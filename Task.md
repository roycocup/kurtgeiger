# Test for Kurt Geiger

## Developer Task

This task is predominantly an exercise where we expect you to showcase your ability to create modular and recyclable code. Feel free to polish anything you create with additional frontend styling – but this is not essential. 

You can complete the task with or without using a framework and using a language of your choice. The task should take you about 2-3 hours, but do not worry if you go over this.

The Task

We require you to build the game of  `Pig` (it’s a dice game) rules can be found below or explained here in this handy video: https://www.youtube.com/watch?v=gVIvB-ahI4A
- We’re expecting the 2 dice version of the game.

## The Rules

1. The game requires two or more players.
2. It requires two 6 sided dice.
3. Each player in turn roles the dice counting up the numeric scores until either:
    1. The Player decides to stop rolling  - in which case the user notes down the total they have accumulated from that turn and adds that to their overall score.
    2. The Player rolls a 1. – They lose any points they accumulated for that round.
    3. The Player rolls two 1’s (snake eyes) and then they lose their points from that round and any points they’ve accumulated from previous rounds. E.g. their score goes back to 0.
4. When any of the point 2 scenarios have happened play passes to the next player and the cycle repeats.
5. A winner is the first person to reach 100 points. 

## Submission

Please return your completed task as zip file with any additional instructions to get the game running. 
We will be asking you questions about this task and also potentially to expand/refactor your code during the interview.