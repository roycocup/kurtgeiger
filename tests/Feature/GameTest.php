<?php

namespace Tests\Feature;

use App\{Game, Player};
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Dice;

class GameTest extends TestCase
{
    public function test_can_get_game_over()
    {
        $game = factory(Game::class)->create();

        $expected = true;

        $this->assertEquals($expected, $game->isGameOver());
    }

    public function test_game_can_retrieve_player_from_players()
    {
        $game = factory(Game::class)->create();
        $players = factory(Player::class, 5)->create();
        $game->players()->sync($players);

        $player = $game->players[0];

        $this->assertInstanceOf(\App\Player::class, $player);
    }

    public function test_game_has_at_least_2_players()
    {
        $game = factory(Game::class)->create([
            'num_players' => 1,
        ]);

        $expected = false;

        $this->assertEquals($expected, $game->hasMinNumPlayers());
    }

    public function test_has_default_status()
    {
        $game = factory(Game::class)->create([]);

        $this->assertEquals(Game::STATUS_SETUP, $game->getStatus());
    }

    public function test_game_can_start_if_provided_a_group()
    {
        $players = factory(Player::class, 10)->create();
        $game = factory(Game::class)->create();

        // the group can be adquired by anyone of the elements of this group
        $group = $players[0]->group;
        
        $game->start($group);

        $this->assertEquals(Game::STATUS_RUNNING, $game->getStatus());
        $this->assertEquals(1, $game->cur_player->id);
        $this->assertEquals(0, $game->score($game->cur_player));
    }

    public function test_game_starts_and_player_does_roll_with_random_score()
    {
        $players = factory(Player::class, 10)->create();
        $game = factory(Game::class)->create();
        $game->start($players[0]->group);
        $dice = new Dice();

        $play = $game->cur_player->rollDice($game, $dice);

        // total score this far
        $score = $game->score($game->cur_player);

        $this->assertIsInt($score);
        $this->assertGreaterThan(0, $score);
    }

    public function test_can_get_play_score()
    {
        $players = factory(Player::class, 10)->create();
        $game = factory(Game::class)->create();
        $game->start($players[0]->group);
        
        // mock dice so we can control the end result
        $dice = \Mockery::mock(\App\Dice::class, \App\IRollable::class)
            ->shouldReceive('roll')
            ->andReturn(['die1' => 4, 'die2' => 2])
            ->mock();
        
        $play = $game->cur_player->rollDice($game, $dice);

        $this->assertEquals(6, $play->getPlayScore());
    }

    public function test_can_get_play_score_for_one()
    {
        $players = factory(Player::class, 10)->create();
        $game = factory(Game::class)->create();
        $game->start($players[0]->group);
        
        // mock dice so we can control the end result
        $dice = \Mockery::mock(\App\Dice::class, \App\IRollable::class)
            ->shouldReceive('roll')
            ->andReturn(['die1' => 3, 'die2' => 1])
            ->mock();
        
        $play = $game->cur_player->rollDice($game, $dice);

        $this->assertEquals(0, $play->getPlayScore());
    }

    public function test_can_get_play_and_total_score_for_snakeeyes()
    {
        $players = factory(Player::class, 10)->create();
        $game = factory(Game::class)->create();
        $game->start($players[0]->group);
        
        // mock dice so we can control the end result
        $diceRoll1 = \Mockery::mock(\App\IRollable::class)
            ->shouldReceive('roll')
            ->andReturn(['die1' => 3,'die2' => 3])
            ->mock();

        $diceRoll2 = \Mockery::mock(\App\IRollable::class)
            ->shouldReceive('roll')
            ->andReturn(['die1' => 3,'die2' => 4])
            ->mock();

        $snakeEyesRoll = \Mockery::mock(\App\IRollable::class)
            ->shouldReceive('roll')
            ->andReturn(['die1' => 1,'die2' => 1])
            ->mock();
        
        // roll them twice to build up some score
        $game->cur_player->rollDice($game, $diceRoll1);
        $game->cur_player->rollDice($game, $diceRoll2);
        $score = $game->score($game->cur_player);
        $this->assertEquals(13, $score);

        // one more roll into snakeeyes
        $game->cur_player->rollDice($game, $snakeEyesRoll);
        $score = $game->score($game->cur_player);
        $this->assertEquals(0, $score);
    }
}
