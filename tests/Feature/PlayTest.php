<?php

namespace Tests\Feature;

use App\Play;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlayTest extends TestCase
{
    public function test_play_has_player()
    {
        $play = factory(Play::class)->create();

        $this->assertNotNull($play->player_id);
    }

    public function test_play_has_game()
    {
        $play = factory(Play::class)->create();

        $this->assertNotNull($play->game_id);
    }


}
