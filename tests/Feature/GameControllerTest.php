<?php

namespace Tests\Feature;

use App\{Game, Player};
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GameControllerTest extends TestCase
{
    public function test_status_is_running()
    {
        $players = factory(Player::class, 3)->create();
        $group = $players[0]->group;
        $this->get('/game/'.$group)->assertOk();
        $game = Game::find(1);
        $this->assertEquals(\App\Game::STATUS_RUNNING, $game->getStatus());
    }
}
