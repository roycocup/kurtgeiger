<?php

namespace Tests\Feature;

use App\Game;
use App\Http\Controllers\StartController;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StartControllerTest extends TestCase
{
    use WithFaker; 
    
    public function test_controller_has_setup_method()
    {
        $this->get('setup')->assertOk();
    }

    public function test_setup_can_save_players_and_game()
    {
        $this->post('setup', [
            'num_players' => 3,
            'player' => [
                ['name' => 'tester1', 'email' => 'unique1@email.com', 'group' => 'xyz'],
                ['name' => 'tester2', 'email' => 'unique2@email.com', 'group' => 'xyz'],
                ['name' => 'tester3', 'email' => 'unique3@email.com', 'group' => 'xyz'],
            ],
        ])->assertStatus(302);

        $this->assertDatabaseHas('players', [
            'name' => 'tester1', 
            'name' => 'tester2', 
            'name' => 'tester3',     
        ]);

        $this->assertNotNull(Game::find(1));
    }
}
