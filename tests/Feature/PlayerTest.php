<?php

namespace Tests\Feature;

use App\{Player, Play, Game};
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlayerTest extends TestCase
{
    public function test_players_have_names()
    {
        $players = factory(Player::class, 3)->create();

        $this->assertNotNull($players[0]->name);
        $this->assertNotNull($players[1]->name);
        $this->assertNotNull($players[2]->name);
    }

    public function test_players_have_emails()
    {
        $players = factory(Player::class, 3)->create();

        $this->assertNotNull($players[0]->email);
        $this->assertNotNull($players[1]->email);
        $this->assertNotNull($players[2]->email);
    }

    public function test_can_roll_dice()
    {
        $game = factory(Game::class)->create();
        $player = factory(Player::class)->create();

        $this->assertIsObject($player->rollDice($game, new \App\Dice()));
    }

    public function test_user_can_get_individual_die_roll_result()
    {
        $player = factory(Player::class)->create();
        $play = factory(Play::class)->create([
            'player_id' => $player,
        ]);
        
        $player->play->roll(new \App\Dice());

        $this->assertTrue( is_int($player->play->die1) );
        $this->assertTrue( is_int($player->play->die2) );
    }

    public function test_players_have_a_common_group()
    {
        $players = factory(Player::class, 10)->create([
            'group' => 'group-xyz',
        ]);

        $this->assertEquals('group-xyz', $players[0]->group);
        $this->assertEquals('group-xyz', $players[1]->group);
    }
}
