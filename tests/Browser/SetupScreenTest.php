<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;

class SetupScreenTest extends DuskTestCase
{
    use WithFaker; 

    public function test_can_fill_fields_and_submit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/setup')
            ->click('#num_players')
            ->select('#num_players', 3)
            ->type('player[1][name]', $this->faker->name)
            ->type('player[1][email]', $this->faker->email)
            ->type('player[2][name]', $this->faker->name)
            ->type('player[2][email]', $this->faker->email)
            ->type('player[3][name]', $this->faker->name)
            ->type('player[3][email]', $this->faker->email)
            ->click('#btn-next')
            ->assertPathBeginsWith('/game')
            ;
        });
    }
}
