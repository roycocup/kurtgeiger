<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class WelcomeScreenTest extends DuskTestCase
{
    public function test_welcome()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Welcome to Pig Game');
        });
    }

    public function test_button_redirect()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->click('#btn-next')
                    ->assertPathIs('/setup');
        });
    }
}
