<?php

namespace Tests\Feature;

use App\Dice;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DieTest extends TestCase
{
    public function test_can_get_rolled_number_from_separate_die()
    {
        $dice = factory(Dice::class)->create();

        $result = $dice->roll();

        $this->assertTrue(  is_array($result  ), 'result it not array');
        $this->assertTrue(  is_int($result['die1'] ), 'first dice did not return an int');
        $this->assertTrue(  is_int($result['die2'] ), 'second dice did not return an int');
    }

    public function test_result_have_number_between_1_and_6()
    {
        $dice = factory(Dice::class)->create();

        $result = $dice->roll();

        $this->assertTrue( $result['die1'] > 0 && $result['die1'] <= 6 );
        $this->assertTrue( $result['die2'] > 0 && $result['die2'] <= 6 );
    }
}
