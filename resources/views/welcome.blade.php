@extends('layouts.app')


@section('content')
    <div class="text-center block">
        <h1>Welcome to Pig Game</h1>
    </div>
    <div class="text-center block">
        <img src="img/white-dice.jpg" alt="white dice" class="">
    </div>
    <div class="text-right">
        <a href="{{route('setup')}}">
            <button id="btn-next" class="bg-blue hover:bg-blue-dark text-white font-bold py-4 px-8 rounded-full">
                Next
            </button>
        </a>
    </div>
@endsection