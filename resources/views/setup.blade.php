@extends('layouts.app')


@section('content')
    <form action="" method="post">
        @csrf
        <div class="text-center block">
            <label for="">
                How many players
            </label>
        </div>
        <div class="text-center block">
            <select name="num_players" id="num_players">
            @for ($i = 2; $i < 20; $i++)
                <option value="{{$i}}">{{$i}}</option>
            @endfor
            </select>
        </div>
        <template v-for="n in 10">
            <div class="block">
                <div class="mx-4">
                    <label for="">Name</label>
                    <input type="text" :name="'player[' + n + '][name]'" class="my-4">
                    <label for="">Email</label>
                    <input type="text" :name="'player[' + n + '][email]'" class="my-4">
                </div>
            </div>
        </template>
        <div class="text-right">
                <button id="btn-next" class="bg-blue hover:bg-blue-dark text-white font-bold py-4 px-8 rounded-full" onclick="submit()">
                    Next
                </button>
            </a>
        </div>
    </form>
@endsection