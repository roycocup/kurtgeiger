# How to run this program
1. Run composer to install all the vendor folders `php composer install`
2. a .env file with pretty much a copy of .env.example except for database location.
3. The database is sqlite so you will not need to have a mysql connection or any settings besides the full path to the sqlite file in your .env file


# Design Gameplay 
* A `game` is played by having some `players` defined in a `group`
* A `group` is a common id for all the players in a given game.
* The `game` starts with a `group` (and implicitly some players).
* The `game` will then be set to `running` and will set the `cur_player` to be the one in turn to roll dice.
* The `game->cur_player` will call `rollDice` and have that roll, recorded in the `play` model.
* The `play` model persists, which game, player and what was the result of that play in each dice.
* if the result from the `play` is not 1 on any of the dice, the `game` remains set as is and allow for more rolls.
* Score is kept by adding the score of the plays for that player. 
* If the `player` does draw a single 1, the `play` should score 0
* If the `player` draws a double 1, then the `play` will score/persist the negative amount of the sum that player has up to this roll, the `game` will set the `cur_player` to the next player.
* If the `player` draws over 100, the game ends. 
* If the `player` draws passes turn, `game` will set the next player as `cur_player`
     

## Wireframes
https://wireframe.cc/M36RvW

## Decisions
### Why Laravel?
At first I thought I wasn't going to use an MVC framework for a game because it seemed a bit like `squaring a circle`. 
But then when considering having layouts, and routes, it made more sense to incorporate some off-the-shelf packages. 
I assume that KG is working mostly with Symfony and my not being an expert on it, and not having any experience with TDD on symfony,I expected that you'd see more flaws in my design than if I used a framework where I know how to do tests. 
Also, I expected to get stuck in multiple issues and I find Laravel's documentation simpler to understand and easier to find what you are looking for whereas Symfony always seemed to me much more fragmented. 
So thats the main reasons why I chose to do this using Laravel, but just to reiterate - I'm an expert in neither and if I had to do this on my own, I would potencially do it using pure php and some design patterns.