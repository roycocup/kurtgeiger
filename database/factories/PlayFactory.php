<?php

use Faker\Generator as Faker;

$factory->define(App\Play::class, function (Faker $faker) {
    return [
        'player_id' => factory(App\Player::class)->create(),
        'game_id' => factory(App\Game::class)->create(),
    ];
});
